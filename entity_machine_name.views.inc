<?php

/**
 * @file
 * Provide views data.
 */

/**
 * Implements hook_views_plugins_argument_validator_alter().
 */
function entity_machine_name_views_plugins_argument_validator_alter(array &$plugins) {
  $plugins['entity_machine_name_term'] = [
    'parent' => 'parent',
    'plugin_type' => 'argument_validator',
    'register_theme' => TRUE,
    'title' => t('Taxonomy term machine name'),
    'short_title' => '',
    'id' => 'entity_machine_name_term',
    'entity_type' => 'taxonomy_term',
    'class' => 'Drupal\entity_machine_name\Plugin\views\argument_validator\EntityMachineNameTerm',
    'provider' => 'entity_machine_name',
  ];
  $plugins['entity_machine_name_node'] = [
    'parent' => 'parent',
    'plugin_type' => 'argument_validator',
    'register_theme' => TRUE,
    'title' => t('Node machine name'),
    'short_title' => '',
    'id' => 'entity_machine_name_node',
    'entity_type' => 'node',
    'class' => 'Drupal\entity_machine_name\Plugin\views\argument_validator\EntityMachineNameNode',
    'provider' => 'entity_machine_name',
  ];
}

/**
 * Implements hook_views_data_alter().
 */
function entity_machine_name_views_data_alter(array &$data) {
  $data['taxonomy_term_field_data']['machine_name']['filter']['id'] = 'entity_term_index_machine_name';
}
