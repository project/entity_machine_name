<?php

namespace Drupal\entity_machine_name\Services;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Entity Machine Name Helper service.
 */
class EntityMachineNameHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The machine name.
   *
   * @var string
   */
  protected string $machineName;

  /**
   * Constructs a new Entity Machine Name Helper.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->machineName = '';
  }

  /**
   * Check and alter machine name to generate a unique value.
   *
   * @param string $machine_name
   *   Machine name to uniquify.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to uniquify.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function uniqueMachineName(string &$machine_name, EntityInterface $entity) {
    /** @var \Drupal\Core\Entity\EntityInterface $existing */
    $existing = $this->entityMachineNameLoad($machine_name, $entity);
    if (!$existing || $existing->id() == $entity->id()) {
      return;
    }

    // If the machine name already exists, generate a new, variant.
    $original_machine_name = $machine_name;

    $i = 0;
    do {
      // Append an incrementing numeric suffix until we find a unique value.
      $unique_suffix = '_' . $i;
      $machine_name = Unicode::truncate(
          $original_machine_name,
          255 - mb_strlen($unique_suffix)
        ) . $unique_suffix;
      $i++;
    } while ($this->entityMachineNameLoad($machine_name, $entity));
  }

  /**
   * Try to map a string to an existing entity, as for glossary use.
   *
   * Provides a case-insensitive and trimmed mapping, to maximize the
   * likelihood of a successful match.
   *
   * @param string $machine_name
   *   The machine name to search for.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to search.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Entity object or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function entityMachineNameLoad(string $machine_name, EntityInterface $entity): ?EntityInterface {
    $entities = [];
    $conditions = [
      'machine_name' => $machine_name,
    ];
    if ($entity instanceof TermInterface) {
      $conditions['vid'] = $entity->bundle();
      $entities = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($conditions);
    }
    if ($entity instanceof NodeInterface) {
      $entities = $this->entityTypeManager->getStorage('node')->loadByProperties($conditions);
    }
    if (!empty($entities) && key($entities) != $entity->id()) {
      return reset($entities);
    }
    return NULL;
  }

  /**
   * Try to map a string to an existing entity type, as for glossary use.
   *
   * Provides a case-insensitive and trimmed mapping, to maximize the
   * likelihood of a successful match.
   *
   * @param string $machine_name
   *   The machine name to search for.
   * @param string $entity_type
   *   The entity type to search.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Entity object or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function entityMachineNameLoadByType(string $machine_name, string $entity_type): ?EntityInterface {
    $conditions = [
      'machine_name' => $machine_name,
    ];
    if (in_array($entity_type, array_keys($this->entityTypeManager->getDefinitions()))) {
      $entities = $this->entityTypeManager->getStorage($entity_type)->loadByProperties($conditions);
      if (!empty($entities)) {
        return reset($entities);
      }
    }
    return NULL;
  }

}
