<?php

namespace Drupal\entity_machine_name\Services;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * The entity machine name form.
 */
class EntityMachineNameForm {

  use StringTranslationTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * The transliterator.
   *
   * @var \Drupal\Core\Transliteration\PhpTransliteration
   */
  protected $transliteration;

  /**
   * The module hadler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The machine name.
   *
   * @var string
   */
  protected $machineName;

  /**
   * The class constructor.
   */
  public function __construct() {
    $this->machineName = '';
  }

  /**
   * Perform alterations before a form is rendered.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function formAlter(array &$form, FormStateInterface $form_state) {
    if ($form_state->getFormObject() instanceof EntityForm) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $form_state->getFormObject()->getEntity();

      if ($entity instanceof TermInterface || $entity instanceof NodeInterface) {
        if ($entity->hasField('machine_name')) {
          if (!empty($machine_name = $entity->get('machine_name')->first())) {
            $this->machineName = $machine_name->getValue()['value'];
          }
        }
        else {
          if ($entity instanceof TermInterface) {
            $this->setMachineNameTerm($entity);
          }
          /** @var \Drupal\node\NodeInterface $entity */
          if ($entity instanceof NodeInterface) {
            $this->setMachineNameNode($entity);
          }
        }

        $form['machine_name'] = [
          '#type' => 'machine_name',
          '#default_value' => entity_machine_name_clean_name($this->machineName),
          '#maxlength' => 255,
          '#machine_name' => [
            'exists' => 'entity_machine_name_exists_load',
          ],
        ];
        if ($entity instanceof TermInterface) {
          $form['machine_name']['#machine_name']['source'] = [
            'name',
            'widget',
            0,
            'value',
          ];
          $form['machine_name']['#weight'] = $form['name']['#weight'] + 0.01;
        }
        if ($entity instanceof NodeInterface) {
          $form['machine_name']['#machine_name']['source'] = [
            'title',
            'widget',
            0,
            'value',
          ];
          $form['machine_name']['#weight'] = $form['title']['#weight'] + 0.01;
        }

        $form['#validate'][] = [$this, 'entityMachineNameFormValidate'];
      }
    }
  }

  /**
   * Entity machine name form validation.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function entityMachineNameFormValidate(array $form, FormStateInterface $form_state) {
    // During the deletion there is no 'machine_name' key.
    if ($form_state->hasValue('machine_name')) {
      // Do not allow machine names to conflict with taxonomy path arguments.
      $machine_name = $form_state->getValue('machine_name');
      $disallowed = ['add', 'list', 'delete', 'update'];

      if (in_array($machine_name, $disallowed)) {
        $form_state->setError($form['machine_name'], $this->t('The machine-readable name cannot be "add", "update", "delete" or "list".'));
      }
    }
  }

  /**
   * Set term machine name.
   *
   * @param \Drupal\taxonomy\TermInterface $entity
   *   The entity.
   */
  protected function setMachineNameTerm(TermInterface $entity) {
    if (!empty($machine_name_value = $entity->get('name')->first()->getValue())) {
      $this->machineName = $machine_name_value['value'];
    }
  }

  /**
   * Set node machine name.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The entity.
   */
  protected function setMachineNameNode(NodeInterface $entity) {
    if (!empty($entity->getTitle())) {
      $this->machineName = $entity->getTitle();
    }
  }

}
