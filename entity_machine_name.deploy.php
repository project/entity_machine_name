<?php

/**
 * @file
 * Deploy functions for the Taxonomy Machine Name module.
 */

use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;

/**
 * Add machine_name for previously created terms.
 */
function entity_machine_name_deploy_update_existing_terms(&$sandbox) {
  if (empty($sandbox['tids'])) {
    // Size of the batch to process.
    $batch_size = 10;

    $tids = \Drupal::entityQuery('taxonomy_term')->notExists('machine_name')->execute();

    $sandbox['total'] = count($tids);
    $sandbox['tids'] = array_chunk($tids, $batch_size);
    $sandbox['succeeded'] = $sandbox['errored'] = $sandbox['processed_chunks'] = 0;
  }

  $entity_type_manager = \Drupal::entityTypeManager();
  $term_storage = $entity_type_manager->getStorage('taxonomy_term');

  // Nothing to do?
  if (!$sandbox['total']) {
    $term_storage->resetCache();
    $sandbox['#finished'] = 1;
    return;
  }

  // Process all terms in this chunk.
  $current_chunk = $sandbox['tids'][$sandbox['processed_chunks']];
  $terms = Term::loadMultiple($current_chunk);

  foreach ($terms as $term) {
    $success = entity_machine_name_entities_update($term);
    $success ? $sandbox['succeeded']++ : $sandbox['errored']++;
  }

  // Increment the number of processed chunks to determine when we've finished.
  $sandbox['processed_chunks']++;

  // When we have processed all of the chunks $sandbox['#finished'] will be 1.
  // Then the batch / update runner will consider the job finished.
  $sandbox['#finished'] = $sandbox['processed_chunks'] / count($sandbox['tids']);

  if ($sandbox['#finished'] >= 1) {
    return t(
      '@succeeded terms were updated correctly. @errored terms failed.',
      [
        '@succeeded' => $sandbox['succeeded'],
        '@errored' => $sandbox['errored'],
      ]
    );
  }
}

/**
 * Add machine_name for previously created nodes.
 */
function entity_machine_name_deploy_update_existing_nodes(&$sandbox) {
  if (empty($sandbox['nids'])) {
    // Size of the batch to process.
    $batch_size = 10;

    $nids = \Drupal::entityQuery('node')->notExists('machine_name')->execute();

    $sandbox['total'] = count($nids);
    $sandbox['nids'] = array_chunk($nids, $batch_size);
    $sandbox['succeeded'] = $sandbox['errored'] = $sandbox['processed_chunks'] = 0;
  }

  $entity_type_manager = \Drupal::entityTypeManager();
  $node_storage = $entity_type_manager->getStorage('node');

  // Nothing to do?
  if (!$sandbox['total']) {
    $node_storage->resetCache();
    $sandbox['#finished'] = 1;
    return;
  }

  // Process all terms in this chunk.
  $current_chunk = $sandbox['nids'][$sandbox['processed_chunks']];
  $nodes = Node::loadMultiple($current_chunk);

  foreach ($nodes as $node) {
    $success = entity_machine_name_entities_update($node);
    $success ? $sandbox['succeeded']++ : $sandbox['errored']++;
  }

  // Increment the number of processed chunks to determine when we've finished.
  $sandbox['processed_chunks']++;

  // When we have processed all of the chunks $sandbox['#finished'] will be 1.
  // Then the batch / update runner will consider the job finished.
  $sandbox['#finished'] = $sandbox['processed_chunks'] / count($sandbox['nids']);

  if ($sandbox['#finished'] >= 1) {
    return t(
      '@succeeded nodes were updated correctly. @errored node failed.',
      [
        '@succeeded' => $sandbox['succeeded'],
        '@errored' => $sandbox['errored'],
      ]
    );
  }

}
